![Build Status](https://gitlab.com/pages/nuxt/badges/master/build.svg)

---

# ГИС Лесная карта

![Logotype](assets/images/logo.svg "ГИС Лесная карта")

## Information

- YKSoft project
- SSR Nuxt.js
- Design: [Figma Forest](https://www.figma.com/file/h4gzFVD8my0WR6Cgh3CkPU/Forest)
