import Vue from 'vue'
import VueLazyload from 'vue-lazyload'
import loading from '~/assets/loading.svg'

Vue.use(VueLazyload, {
  preLoad: 1,
  error: loading,
  loading: loading,
  attempt: 1
})
