import imageminWebp from 'imagemin-webp'
import pkg from './package'

const { CI_PAGES_URL } = process.env
// eslint-disable-next-line no-unused-vars
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

export default {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.projectTitle,
    meta: [
      { charset: 'utf-8' },
      { name: 'apple-itunes-app', content: 'app-id=myAppStoreID' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
      { name: 'application-name', content: pkg.projectTitle },
      { name: 'msapplication-TileColor', content: '#FFFFFF' },
      { name: 'msapplication-TileImage', content: '/images/favicon/mstile-144x144.png' },
      { name: 'msapplication-square70x70logo', content: '/images/favicon/mstile-70x70.png' },
      { name: 'msapplication-square150x150logo', content: '/images/favicon/mstile-150x150.png' },
      { name: 'msapplication-wide310x150logo', content: '/images/favicon/mstile-310x150.png' },
      { name: 'msapplication-square310x310logo', content: '/images/favicon/mstile-310x310.png' }
    ],
    link: [
      { rel: 'preconnect', href: 'https://fonts.gstatic.com/s/roboto/v20/' },
      { rel: 'preconnect', href: 'https://fonts.googleapis.com/' },
      { rel: 'preload', href: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,700&display=swap&subset=cyrillic' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,700&display=swap&subset=cyrillic' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'icon', type: 'image/png', href: '/images/favicon/favicon-196x196.png', size: '196x196' },
      { rel: 'icon', type: 'image/png', href: '/images/favicon/favicon-96x96.png', size: '96x96' },
      { rel: 'icon', type: 'image/png', href: '/images/favicon/favicon-32x32.png', size: '32x32' },
      { rel: 'icon', type: 'image/png', href: '/images/favicon/favicon-16x16.png', size: '16x16' },
      { rel: 'icon', type: 'image/png', href: '/images/favicon/favicon-128.png', size: '128x128' },
      { rel: 'apple-touch-icon-precomposed', href: '/images/favicon/apple-touch-icon-57x57.png', size: '57x57' },
      { rel: 'apple-touch-icon-precomposed', href: '/images/favicon/apple-touch-icon-114x114.png', size: '114x114' },
      { rel: 'apple-touch-icon-precomposed', href: '/images/favicon/apple-touch-icon-72x72.png', size: '72x72' },
      { rel: 'apple-touch-icon-precomposed', href: '/images/favicon/apple-touch-icon-144x144.png', size: '144x144' },
      { rel: 'apple-touch-icon-precomposed', href: '/images/favicon/apple-touch-icon-60x60.png', size: '60x60' },
      { rel: 'apple-touch-icon-precomposed', href: '/images/favicon/apple-touch-icon-120x120.png', size: '120x120' },
      { rel: 'apple-touch-icon-precomposed', href: '/images/favicon/apple-touch-icon-76x76.png', size: '76x76' },
      { rel: 'apple-touch-icon-precomposed', href: '/images/favicon/apple-touch-icon-152x152.png', size: '152x152' },
      { rel: 'preload', href: '/images/firstScreen.webp', as: 'image' },
      { rel: 'preload', href: '/images/screen.webp', as: 'image' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: {
    color: '#428B00',
    height: '3px'
  },

  /*
  ** Customize the generated output folder
  */
  generate: {
    dir: 'public'
  },

  /*
  ** Customize the base url
  */
  router: {
    base: '/'
  },

  /*
  ** Global CSS
  */
  css: [
    '~assets/main.scss',
    'normalize.css/normalize.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~plugins/vue-lazyload'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    [
      'nuxt-imagemin',
      {
        plugins: [
          imageminWebp({ quality: 50 })
        ]
      },
      {
        optipng: { optimizationLevel: 5 },
        gifsicle: { optimizationLevel: 2 }
      }
    ]
  ],

  cache: {
    max: 2000,
    maxAge: 31557600
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
