export const state = () => ({
  modalShow: false,
  modalText: '',
  showItem: false
})

export const mutations = {
  openModal(state, text) {
    state.modalShow = true
    state.modalText = text
    document.body.style.overflow = 'hidden'
  },
  closeModal(state) {
    state.modalShow = false
    state.modalText = ''
    document.body.style.overflow = 'auto'
  }
}

export const getters = {
  modalShow: state => state.modalShow,
  modalText: state => state.modalText
}
